# Gitlab Event Listener

The Gitlab event listener will start up a HTTP server and listen for [events](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html) sent from Gitlab and forward them to [Fluentd](https://www.fluentd.org) for further processing.

## Prerequisites

### Traefik

This project makes use of [Traefik](https://traefik.io) to route ingress traffic to the Gitlab event listener service. You can deploy the following [project](https://gitlab.com/docker-swarm-example/ingress/traefik-ingress) to get Traefik up and running in your Docker swarm.

### Fluentd

All application logs produced by the Gitlab event listener are by default sent to Fluentd. You can reconfigure this by changing the logging option in the docker-compose.yml file. If you do want to use Fluentd as your application log handler, you can use this [project](https://gitlab.com/docker-swarm-example/logging/efk-logging.git) to deploy Fluentd to your Docker swarm.

## Gitlab Configuration

To configure Gitlab to send events to the [Gitlab event listener](https://gitlab.com/docker-swarm-example/continuous-integration/gitlab-event-listener) service, you have to go to the project that you want to receive events from and click on Settings -> Integrations. Next fill in the URL to the to your Docker swarm e.g. https://your.domain/gitlab-event-listener. In the "Secret Token" field enter the token that you configured in your [Gitlab event listener](https://gitlab.com/docker-swarm-example/continuous-integration/gitlab-event-listener) configuration (by default this is set to "secret"). Under "Trigger" select the types of events that you want to receive from Gitlab, check the box for "Push events" to receive events for commits that is pushed to Gitlab. Next click on the "Add webhook" button. Now you should see your new webook listed under the "Add webhook" button. To test and see if the webhook works as expected, click on the "Test" drop-down menu and click on "Push events". This will send an example event to your service, so you can verify that everything works as expected without the need to push a real commit to your project.

## Deploy

Execute the following commands to deploy the Gitlab event listener to Docker swarm:

```sh
git clone https://gitlab.com/docker-swarm-example/continuous-integration/gitlab-event-listener.git
docker stack deploy --compose-file gitlab-event-listener/docker-compose.yml gitlab-event-listener
```

