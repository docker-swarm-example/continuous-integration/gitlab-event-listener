export default {
  BASE_PATH: process.env.BASE_PATH || '/webhook',
  SECRET: process.env.SECRET || 'secret',
  SERVER_PORT: process.env.SERVER_PORT || 7777,
  FLUENTD_HOST: process.env.FLUENTD_HOST || 'fluentd',
  FLUENTD_PORT: process.env.FLUENTD_HOST || 24224,
  FLUENTD_TIMEOUT: process.env.FLUENTD_HOST || 3.0,
  FLUENTD_RECONNECT_INTERVAL: process.env.FLUENTD_HOST || 60000,
};
