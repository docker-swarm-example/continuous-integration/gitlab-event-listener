import config from './config';
const logger = require('fluent-logger');

logger.configure('gitlab-event-listener', {
  host: config.FLUENTD_HOST,
  port: config.FLUENTD_PORT,
  timeout: config.FLUENTD_TIMEOUT,
  reconnectInterval: config.FLUENTD_RECONNECT_INTERVAL,
});

export default logger;
