import { CommitEvent } from './commit-event';
import {
  EventData,
  PushEvent,
  Commit,
} from 'node-gitlab-webhook/interfaces';

class PushEventTransformer {

  transform(event: EventData<PushEvent>): EventData<CommitEvent>[] {
    // Transform a single push event into multiple events, where each event represents one commit.
    // So if the user pushed 3 commits in a single git push we will return 3 events.
    return event.payload.commits.map((commit: Commit) => {
      const commitEvent: CommitEvent = {
        commit,
        object_kind: 'commit',
        ref: event.payload.ref,
        user_id: event.payload.user_id,
        user_name: event.payload.user_name,
        user_username: event.payload.user_username,
        user_email: event.payload.user_email,
        user_avatar: event.payload.user_avatar,
        project_id: event.payload.project_id,
        project: event.payload.project,
        repository: event.payload.repository,
      };

      return {
        payload: commitEvent,
        event: event.event,
        protocol: event.protocol,
        host: event.host,
        url: event.url,
        path: event.path,
      };
    });
  }

}

export default new PushEventTransformer();
