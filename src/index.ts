import pushEventTransformer from './push-event-transformer';
import fluentdForwarder from './fluentd-forwarder';
import config from './config';
import * as Http from 'http';
import nodeGitlabWebhook from 'node-gitlab-webhook';
import { CommitEvent } from './commit-event';
import {
  EventData,
  PushEvent,
} from 'node-gitlab-webhook/interfaces';

console.log('Application started.');
console.log('Config:', JSON.stringify(config, null, 2));

const handler = nodeGitlabWebhook({
  path: config.BASE_PATH,
  secret: config.SECRET,
});

Http.createServer((req: Http.IncomingMessage, res: Http.ServerResponse) => {
  console.log(`Incoming "${req.method}" request on url "${req.url}".`);

  handler(req, res, (error: Error) => {
    console.error('An error occurred while handling request.', error);
    res.statusCode = 404;
    res.end('no such location');
  });
}).listen(config.SERVER_PORT);

// Handle webhook events received from Gitlab.
// https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
handler.on('tag_push', fluentdForwarder);
handler.on('issue', fluentdForwarder);
handler.on('note', fluentdForwarder);
handler.on('merge_request', fluentdForwarder);
handler.on('wiki_page', fluentdForwarder);
handler.on('pipeline', fluentdForwarder);
handler.on('build', fluentdForwarder);
handler.on('push', (event: EventData<PushEvent>) => {
  const commits: EventData<CommitEvent>[] = pushEventTransformer.transform(event);

  // Send each commit as a separate event to fluentd.
  commits.forEach(fluentdForwarder);

  // Send the original "push event" to fluentd aswell.
  fluentdForwarder(event);
});

handler.on('error', error => console.error('An unexpected error occurred.', error));
