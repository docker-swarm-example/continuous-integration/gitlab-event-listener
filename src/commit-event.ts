import {
  Project,
  Repository,
  Commit,
} from 'node-gitlab-webhook/interfaces';

interface CommitEvent {
  object_kind: string;
  ref: string;
  user_id: number;
  user_name: string;
  user_username: string;
  user_email: string;
  user_avatar: string;
  project_id: number;
  project: Project;
  repository: Repository;
  commit: Commit;
}

export {
  CommitEvent,
};
