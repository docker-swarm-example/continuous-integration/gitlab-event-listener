import fluentdLogger from './fluentd-logger';
import { CommitEvent } from './commit-event';
import {
  EventData,
  PushEvent,
  TagPushEvent,
  IssueEvent,
  NoteEvent,
  MergeRequestEvent,
  WikiPageEvent,
  PipelineEvent,
  BuildEvent,
} from 'node-gitlab-webhook/interfaces';

export default (event: EventData<PushEvent> |
  EventData<TagPushEvent> |
  EventData<IssueEvent> |
  EventData<NoteEvent> |
  EventData<MergeRequestEvent> |
  EventData<WikiPageEvent> |
  EventData<PipelineEvent> |
  EventData<BuildEvent> |
  EventData<CommitEvent>) => {

  console.log(`Sending ${event.payload.object_kind} event to fluentd.`);
  fluentdLogger.emit(event.payload.object_kind, event);
};
